package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public class HighSpiritSealSpell extends HighSpiritSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
